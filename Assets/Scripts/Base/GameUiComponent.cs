﻿using Interfaces;
using UnityEngine;

public class GameUiComponent : MonoBehaviour, IGameUi
{
    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}