﻿namespace Interfaces
{
    public interface IGameUi
    {
        void Show();
        void Hide();
    }
}
