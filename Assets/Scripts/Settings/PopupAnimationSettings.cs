﻿using UnityEngine;

[CreateAssetMenu(fileName = "PopupAnimationSettings", menuName = "PopupAnimation/PopupAnimationSettings", order = 1)]
public class PopupAnimationSettings : ScriptableObject
{
    public float ScaleDuration = 0.5f;
    public float FadeOutDuration = 0.5f;
    public float MovingDuration = 1.0f;
    
    public float ScaleInFactor = 1.5f;
    public float ScaleOutFactor = 0f;
}
