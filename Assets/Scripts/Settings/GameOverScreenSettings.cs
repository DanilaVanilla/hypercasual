﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameOverAnimationSettings", menuName = "GameOverAnimation/GameOverAnimationSettings", order = 2)]
public class GameOverScreenSettings : ScriptableObject
{
    public Color NewScoreColorText;
    public Color NewScoreColorBoard;
}
