﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : GameUiComponent
{
    [SerializeField] private Button _restartButton;
    [SerializeField] private GameObject _gameOverTitle;

    [SerializeField] private Image _scoreBoard;

    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _scoreTitleText;
    
    [SerializeField] private Text _bestScoreText;
    [SerializeField] private Text _bestScoreTitleText;

    [SerializeField] private GameOverScreenSettings _screenSettings;

    private const float MovingAnimationDuration = 0.5f;

    private void Start()
    {
        _restartButton.onClick.AddListener(OnPlayButton);
    }

    public void Bind(Score score)
    {
        if (score.IsNewBestScore())
        {
            score.UpdateBestScore();

            _scoreText.color = _screenSettings.NewScoreColorText;
            _scoreTitleText.color = _screenSettings.NewScoreColorText;
            
            _bestScoreText.color = _screenSettings.NewScoreColorText;
            _bestScoreTitleText.color = _screenSettings.NewScoreColorText;

            _scoreBoard.color = _screenSettings.NewScoreColorBoard;
        }

        _scoreText.text = score.GetScore().ToString();
        _bestScoreText.text = Score.GetBestScore().ToString();

        Animate();
    }

    private void Animate()
    {
        var sequence = DOTween.Sequence();

        sequence.Append(AnimateMoving(_gameOverTitle.transform));
        sequence.Append(AnimateMoving(_scoreBoard.transform));
        sequence.Append(AnimateMoving(_restartButton.transform));
    }

    private Tween AnimateMoving(Transform itemTransform)
    {
        var tween = itemTransform.DOLocalMove(
            new Vector3(0, itemTransform.localPosition.y, itemTransform.localPosition.z), MovingAnimationDuration);

        return tween;
    }

    private void OnPlayButton()
    {
        Hide();
        Utils.ReloadCurrentScene();
    }
}