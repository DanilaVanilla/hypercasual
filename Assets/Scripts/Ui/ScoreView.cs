﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreView : MonoBehaviour
{
    [SerializeField] private Text _scoreText;

    public void SetScoreText(int amount)
    {
        _scoreText.text = amount.ToString("00");
    }
}
