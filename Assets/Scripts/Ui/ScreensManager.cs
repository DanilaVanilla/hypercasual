﻿using UnityEngine;

public class ScreensManager : MonoBehaviour
{
    private GameUiComponent[] _objects;
    
    private int _objectCount;
    
    public static ScreensManager Instance { get; private set; }
    
    private void Awake()
    {
        Instance = this;
    }

    public void Start()
    {
        _objects = GetComponentsInChildren<GameUiComponent>(true);
        _objectCount = _objects.Length;
    }

    private T Get<T>() where T : GameUiComponent
    {
        for (var index = 0; index < _objectCount; ++index)
        {
            if (_objects[index] is T) return (T) _objects[index];
        }
        return default (T);
    }

    public T Show<T>() where T : GameUiComponent
    {
        var obj = Get<T>();
        if (obj == null) return default (T);
        
        obj.Show();

        return obj;
    }
    
    public T ShowOverAll<T>() where T : GameUiComponent
    {
        HideAll();
        
        var obj = Get<T>();
        if (obj == null) return default (T);
        
        obj.Show();
        
        return obj;
    }

    private void HideAll()
    {
        foreach (var gameUiComponent in _objects)
        {
            gameUiComponent.Hide();
        }
    }
}
