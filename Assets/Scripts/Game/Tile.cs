﻿using System.Collections;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] private float _fallingDelay = 0.5f;
    [SerializeField] private float _disableDelay = 1.5f;
    
    [SerializeField] private Pickup _pickUp;
    
    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Player>())
        {
            TileManager.Instance.SpawnTile();
            StartCoroutine(StartFallCoroutine());
        }
    }
    
    private IEnumerator StartFallCoroutine()
    {
        yield return Yielders.Get(_fallingDelay);
        
        EnableFalling();
        
        yield return Yielders.Get(_disableDelay);
        
        Disable();
        DisableFalling();
        
    }

    private void EnableFalling()
    {
        _rigidbody.isKinematic = false;
    }
    
    private void DisableFalling()
    {
        _rigidbody.isKinematic = true;
    }
    
    public void Disable()
    {
        gameObject.SetActive(false);
    }
    
    public void Enable()
    {
        gameObject.SetActive(true);
    }
    
    public void EnablePickup()
    {
        _pickUp.Enable();
    }

    public void DisablePickup()
    {
        _pickUp.Disable();
    }
}