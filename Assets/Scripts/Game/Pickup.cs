﻿using UnityEngine;

public class Pickup : MonoBehaviour
{
    [SerializeField] private float _angle = 1f;
    [SerializeField] private float _rotationSpeed = 50f;
    
    private void Update()
    {
        Rotate();
    }

    private void Rotate()
    {
        transform.Rotate(Vector3.up, _angle * _rotationSpeed * Time.deltaTime);
    }

    public void Collect()
    {
        Disable();
    }
    
    public void Disable()
    {
        gameObject.SetActive(false);
    }
    
    public void Enable()
    {
        gameObject.SetActive(true);
    }
}