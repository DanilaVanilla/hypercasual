﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TilePool : MonoBehaviour
{
    [SerializeField] private Tile _prefab;
    
    private const int StartedLength = 25;
    private readonly List<Tile> _items = new List<Tile>();
   
    public void Initialize()
    {
        CreateTiles();    
    }
    
    private void CreateTiles()
    {
        for (var i = 0; i < StartedLength; i++)
        {
            _items.Add(InstantiateTile());
        }
    }

    private Tile InstantiateTile()
    {
        var tile = Instantiate(_prefab, transform);
        tile.gameObject.SetActive(false);

        return tile;
    }
    
    public Tile GetTile()
    {
        var tile = _items.FirstOrDefault(t => !t.gameObject.activeSelf);
        if (tile == null)
        {
            tile = InstantiateTile();
        }
        
        tile.Enable();
        tile.DisablePickup();
        
        return tile;
    }
}