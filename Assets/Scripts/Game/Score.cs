﻿using System;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private ScoreView _view;

    private int _score;

    private const string BestScorePlayerPrefsKey = "BEST_SCORE_KEY";

    private static int BestScore
    {
        set
        {
            PlayerPrefs.SetInt(BestScorePlayerPrefsKey, value);
        }

        get
        {
            return PlayerPrefs.GetInt(BestScorePlayerPrefsKey, 0);
        }
    }
    
    private void Start()
    {
        Reset();
    }

    private void Reset()
    {
        _score = 0;
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        _view.SetScoreText(_score);
    }
    
    public void IncrementScore()
    {
        _score++;
       UpdateScoreText();
    }
    
    public void AddScore(int amount)
    {
        if (amount < 0)
        {
            throw new ArgumentOutOfRangeException();
        }
        
        _score += amount;
        UpdateScoreText();
    }

    public int GetScore()
    {
        return _score;
    }

    public bool IsNewBestScore()
    { 
        return _score > BestScore;;
    }

    public void UpdateBestScore()
    {
        BestScore = _score;
    }

    public static int GetBestScore()
    {
        return BestScore;
    }
}