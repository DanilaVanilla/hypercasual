﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileManager : MonoBehaviour
{
    [SerializeField] private Tile _startedTile;
    [SerializeField] private TilePool _tilePool;
    
    [SerializeField] private float _scaleX = 3f;
    [SerializeField] private float _scaleZ = 3f;
    
    private Tile _currentTile;

    private const int StartedLength = 10;
    private const int PickupMaxRange = 10;
    
    public static TileManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        _tilePool.Initialize();
        _currentTile = _startedTile;

        for (var i = 0; i < StartedLength; i++)
        {
            SpawnTile();
        }
    }

    public void SpawnTile()
    {
        var randomDirection = GetRandomDirection();

        _currentTile = randomDirection == Direction.Left ? InstantiateLeft() : InstantiateTop();
        
        if (IsSpawnPickup())
        {
            _currentTile.EnablePickup();
        }
    }
    
    private Tile InstantiateLeft()
    {
        var position = _currentTile.transform.position + Vector3.left * _scaleX;

        return InstantiateTile(position);
    }
    
    private Tile InstantiateTop()
    {
        var position = _currentTile.transform.position + Vector3.forward * _scaleZ;

        return InstantiateTile(position);
    }

    private Tile InstantiateTile(Vector3 position)
    {
        var tile = _tilePool.GetTile();
        tile.transform.position = position;

        return tile;
    }
    
    private static bool IsSpawnPickup()
    {
        var random = Random.Range(0, PickupMaxRange);

        return random == 0;
    }

    private static Direction GetRandomDirection()
    {
        var values = Enum.GetValues(typeof(Direction));
        var randomDirection = (Direction) values.GetValue(Random.Range((int) Direction.Up, values.Length));

        return randomDirection;
    }
}