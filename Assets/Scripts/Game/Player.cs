﻿using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Player : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 5f;
    [SerializeField] private Camera _camera;

    [SerializeField] private Score _score;
    [SerializeField] private PopupController _popupController;
    [SerializeField] private Transform _contactPoint;

    private const int ScoreByPickup = 3;

    private Rigidbody _rigidbody;

    private bool _isAlive = true;
    private Direction _currentDirection;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();

        _currentDirection = Direction.Stop;
    }

    private void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (!_isAlive) return;

        if (Input.GetMouseButtonDown(0))
        {
            ChangeDirection();
            _score.IncrementScore();
        }
    }

    private void ChangeDirection()
    {
        var directionIsUp = _currentDirection == Direction.Up;
        _currentDirection = directionIsUp ? Direction.Left : Direction.Up;
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void MovePlayer()
    {
        _rigidbody.MovePosition(_rigidbody.position +
                                (_currentDirection.GetDirectionVector() * _moveSpeed * Time.fixedDeltaTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        var pickup = other.gameObject.GetComponent<Pickup>();
        if (pickup)
        {
            pickup.Collect();
            _score.AddScore(ScoreByPickup);

            var scorePopup = _popupController.Create(other.transform.position, ScoreByPickup);
            scorePopup.transform.LookAt(2 * scorePopup.transform.position - _camera.transform.position);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Tile>())
        {
            if (!IsOnLand())
            {
                Dead();
                ShowRestartScreen();
            }
        }
    }

    private bool IsOnLand()
    {
        return OverlapSphere();
    }

    private bool OverlapSphere()
    {
        var playerLayer = gameObject.layer;

        var layerMask = 1 << playerLayer;
        layerMask = ~layerMask;

        var hitColliders = Physics.OverlapSphere(_contactPoint.position, 0.5f, layerMask);

        return hitColliders.Length > 0;
    }

    private void Dead()
    {
        _isAlive = false;
        _camera.transform.parent = null;
    }

    private void ShowRestartScreen()
    {
        ScreensManager.Instance.Show<GameOverScreen>().Bind(_score);
    }
}