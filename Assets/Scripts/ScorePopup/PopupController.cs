﻿using UnityEngine;

public class PopupController : MonoBehaviour
{
    [SerializeField] private ScorePopup _popupPrefab;
    [SerializeField] private PopupAnimationSettings _popupAnimationSettings;

    public ScorePopup Create(Vector3 position, int scoreAmount)
    {
        var damagePopup = Instantiate(_popupPrefab, position, Quaternion.identity, transform);
        
        damagePopup.Setup(scoreAmount, _popupAnimationSettings);
        damagePopup.Animate();

        return damagePopup;
    }
}