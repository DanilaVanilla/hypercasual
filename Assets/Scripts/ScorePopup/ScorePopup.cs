﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using TMPro;

public class ScorePopup : MonoBehaviour
{
    [SerializeField] private Vector3 _offset;

    private TextMeshPro _textMesh;
    private PopupAnimationSettings _popupScoreSettings;

    private void Awake()
    {
        _textMesh = transform.GetComponent<TextMeshPro>();
    }

    public void Setup(int scoreAmount, PopupAnimationSettings popupAnimationSettings)
    {
        _popupScoreSettings = popupAnimationSettings;

        SetText(scoreAmount.ToString());
    }

    public void Animate()
    {
        var sequence = DOTween.Sequence();
        
        sequence.Append(AnimateMoving());

        var scalingSequence = AnimateScaling();
        scalingSequence.Append(AnimateFadeOut());
        
        sequence.Join(AnimateScaling());
        sequence.OnComplete(Destroy);
    }

    private Tween AnimateMoving()
    {
        var endPoint = transform.position + _offset;

        return transform.DOMove(endPoint, _popupScoreSettings.MovingDuration);
    }

    private Sequence AnimateScaling()
    {
        var scalingSequence = DOTween.Sequence();
        
        scalingSequence.Append(ScaleIn());
        scalingSequence.Append(ScaleOut());
        
        return scalingSequence;
    }

    private Tween ScaleIn()
    {
        var endValue = Vector3.one * _popupScoreSettings.ScaleInFactor;

        return transform.DOScale(endValue, _popupScoreSettings.ScaleDuration);
    }

    private Tween ScaleOut()
    {
        var endValue = Vector3.one * _popupScoreSettings.ScaleOutFactor;

        return transform.DOScale(endValue, _popupScoreSettings.ScaleDuration);
    }

    private Tween AnimateFadeOut()
    {
        return _textMesh.DOFade(0, _popupScoreSettings.FadeOutDuration);
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }

    private void SetText(string score)
    {
        _textMesh.text = '+' + score;
    }
}