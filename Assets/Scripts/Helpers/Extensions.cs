﻿using UnityEngine;

public static class Extensions
{ 
    public static Vector3 GetDirectionVector(this Direction direction)
    {
        switch (direction)
        {
            case Direction.Left:
                return Vector3.left;
            case Direction.Up:
                return Vector3.forward;
            case Direction.Stop:
                return Vector3.zero;
            default:
                return Vector3.zero;
        }
    }
}