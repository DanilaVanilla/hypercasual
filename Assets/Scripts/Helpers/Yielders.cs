﻿using System.Collections.Generic;
using UnityEngine;

public static class Yielders {
 
    private static readonly Dictionary<float, WaitForSeconds> TimeInterval = new Dictionary<float, WaitForSeconds>(100);
 
    public static WaitForSeconds Get(float seconds)
    {
        if (!TimeInterval.ContainsKey(seconds))
        {
            TimeInterval.Add(seconds, new WaitForSeconds(seconds));
        }

        return TimeInterval[seconds];
    }
}